package method_drill;

public class Practice18 {

	static String getMessage(String name, boolean isKid) {

		if (isKid) {
			return "こんにちは。" + name + "ちゃん。";
		} else {
			return "こんにちは。" + name + "さん。";
		}
	}

	public static void main(String[] args) {

		System.out.println(getMessage("endo", false));

	}
}
