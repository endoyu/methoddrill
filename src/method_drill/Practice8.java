package method_drill;

public class Practice8 {

	static void printRectangleArea(double height, double width) {

		double rectangleArea = height * width;

		System.out.println(rectangleArea);

	}

	public static void main(String[] args) {

		printRectangleArea(2.5, 10.0);

	}
}
