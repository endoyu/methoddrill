package method_drill;

public class Practice27 {

	static Person getYoungestPerson(Person... persons)
	{
		Person youngestP = persons[0];

		for (int i = 1; i < persons.length; i++)
		{
			if (persons[i].getAge() < youngestP.getAge() || persons[i].getAge() == youngestP.getAge())
			{
				youngestP = persons[i];
			}
		}

		return youngestP;
	}

	public static void main(String[] args)
	{
		Person p0 = new Person("endo", 29);
		Person p1 = new Person("sendo", 20);
		Person p2 = new Person("kendo", 15);
		Person p3 = new Person("mendo", 15);

		Person youngestP = getYoungestPerson(p0, p1, p2, p3);
		String youngestName = youngestP.getName();
		int youngestAge = youngestP.getAge();

		System.out.printf("最年少は%sさんで、年齢は%d歳です。", youngestName, youngestAge);
	}
}
