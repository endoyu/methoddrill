package method_drill;

public class Practice16 {

	static double getMinValue(double a, double b) {
		return Math.min(a, b);
	}

	public static void main(String[] args) {

		System.out.println(getMinValue(2.0, 5.0));

	}
}
