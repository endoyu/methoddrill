package method_drill;

public class Practice6 {

	static void printRandomMessage(String name) {

		int n = (int) (3 * Math.random());

		switch (n) {
			case 0:
				System.out.printf("こんばんは%sさん", name);
				break;
			case 1:
				System.out.printf("こんにちは%sさん", name);
				break;
			case 2:
				System.out.printf("おはよう%sさん", name);
				break;
		}
	}

	public static void main(String[] args) {

		printRandomMessage("endo");

	}
}
