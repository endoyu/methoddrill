package method_drill;

public class Practice19 {

	static int getMinValue(int[] array) {

		int minValue = array[0];

		for (int i = 1; i < array.length; i++) {
			minValue = Math.min(minValue, array[i]);
		}

		return minValue;
	}

	public static void main(String[] args) {

		int[] array = {4, 3, 2, 5, 6, 7, 1};

		System.out.println(getMinValue(array));

	}
}
