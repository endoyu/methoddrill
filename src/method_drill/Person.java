package method_drill;

public class Person {

	private String name;
	private int age;

	Person(String name, int age)
	{
		this.name = name;
		this.age = age;
	}

	String getName()
	{
		return name;
	}
	void setName(String name)
	{
		this.name = name;
	}

	int getAge()
	{
		return age;
	}
	void setAge(int age)
	{
		if (age < 0)
		{
			return;
		}

		this.age = age;
	}

	boolean isSameAge(Person person)
	{
		if (this.age != person.getAge())
		{
			return false;
		}
		return true;
	}


	public static void main(String[] args)
	{
		Person p = new Person("endo", 29);
		System.out.printf("pの名前は%s、%d歳です。%n", p.getName(), p.getAge());

		p.setName("mendo");
		System.out.printf("pは%sに改名しました。%n", p.getName());

		p.setAge(30);
		System.out.printf("pは%d歳になりました。%n", p.getAge());

		Person p1 = new Person("kendo", 10);
		System.out.println(p.isSameAge(p1));
	}
}
