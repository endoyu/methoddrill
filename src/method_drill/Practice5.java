package method_drill;

public class Practice5 {

	static void printCircleArea(double radius) {

		double circleArea = radius * radius * Math.PI;
		System.out.println(circleArea);

	}

	public static void main(String[] args) {

		printCircleArea(2.0);

	}
}
