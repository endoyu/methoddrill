package method_drill;

public class Practice17 {

	static boolean isSameAbsoluteValue(int i, int j) {

		if (Math.abs(i) != Math.abs(j)) {
			return false;
		}
		return true;

	}

	public static void main(String[] args) {

		System.out.println(isSameAbsoluteValue(-4, 4));

	}
}
