package method_drill;

public class Practice11 {

	static String getWeatherForecast() {

		int dayNumber = (int) (Math.random() * 3);
		int weatherNumber = (int) (Math.random() * 4);

		String day = null;

		switch (dayNumber) {
			case 0:
				day = "今日";
				break;
			case 1:
				day = "明日";
				break;
			case 2:
				day = "明後日";
				break;
		}

		String weather = null;

		switch (weatherNumber) {
			case 0:
				weather = "晴れ";
				break;
			case 1:
				weather = "曇り";
				break;
			case 2:
				weather = "雨";
				break;
			case 3:
				weather = "雪";
				break;
		}

		return day + "の天気は" + weather + "でしょう。";
	}

	public static void main(String[] args) {

		System.out.println(getWeatherForecast());

	}
}
