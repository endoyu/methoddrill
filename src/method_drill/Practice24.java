package method_drill;

public class Practice24 {

	static Point getBarycenter(Point... points)
	{
		double sumX = 0.0;
		double sumY = 0.0;

		for (Point p : points)
		{
			sumX += p.x;
			sumY += p.y;
		}

		double px = sumX / points.length;
		double py = sumY / points.length;

		return new Point(px, py);
	}

	public static void main(String[] args)
	{
		Point p0 = new Point(1.1, 2.2);
		Point p1 = new Point(2.2, 3.3);
		Point p2 = new Point(3.3, 4.4);
		Point p3 = new Point(4.4, 5.5);

		Point[] points = {p0, p1, p2, p3};
		Point pc = getBarycenter(points);

		System.out.printf("重心のx座標は%.3f、y座標は%.3fです。", pc.x, pc.y);
	}
}
