package method_drill;

public class Practice13 {

	static String getRandomMessage(String name) {

		int messageNumber = (int) (Math.random() * 3);
		String message = null;

		switch (messageNumber) {
			case 0:
				message = "こんばんは" + name + "さん";
				break;
			case 1:
				message = "こんにちは" + name + "さん";
				break;
			case 2:
				message = "おはよう" + name + "さん";
				break;
		}

		return message;
	}

	public static void main(String[] args) {

		System.out.println(getRandomMessage("endo"));

	}
}
