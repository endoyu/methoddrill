package method_drill;

public class Practice21 {

	static String getLongestString(String... array)
	{
		String str = array[0];

		for (int i = 1; i < array.length; i++)
		{
			if (str.length() < array[i].length() || str.length() == array[i].length())
			{
				str = array[i];
			}
		}

		return str;
	}

	public static void main(String[] args)
	{
		String[] array = {"hello", "hey", "bye", "see you", "hey you"};

		System.out.println(getLongestString(array));
	}
}
