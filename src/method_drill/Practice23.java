package method_drill;

public class Practice23 {

	static double getDistanceBetweenTwoPoints(Point p0, Point p1)
	{
		double dx = p0.x - p1.x;
		double dy = p0.y - p1.y;

		return Math.sqrt(dx * dx + dy * dy);
	}

	public static void main(String[] args)
	{
		Point p0 = new Point(2.2, 4.4);
		Point p1 = new Point(1.1, 3.3);

		System.out.println(getDistanceBetweenTwoPoints(p0, p1));
	}
}
