package method_drill;

public class Practice20 {

	static double getAverage(double... array) {

		double sum = 0.0;

		for (double i : array)
		{
			sum += i;
		}

		return sum / array.length;

	}

	public static void main(String[] args) {

		System.out.println(getAverage(2.2, 4.4, 6.6, 8.8));

	}
}
