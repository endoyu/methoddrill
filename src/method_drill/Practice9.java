package method_drill;

public class Practice9 {

	static void printMaxValue(double a, double b, double c) {

		double[] values = {a, b, c};
		double maxValue = values[0];

		for (int i = 1; i < values.length; i++) {
			maxValue = Math.max(maxValue, values[i]);
		}
		System.out.println(maxValue);
	}

	public static void main(String[] args) {

		printMaxValue(2.2, 10.0, 8.8);

	}
}
