package method_drill;

public class Practice15 {

	static boolean isEvenNumber(int value) {

		if ((value % 2) != 0) {
			return false;
		}
		return true;
	}

	public static void main(String[] args) {

		System.out.println(isEvenNumber(4));
		System.out.println(isEvenNumber(3));

	}
}
