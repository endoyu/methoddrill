package method_drill;

public class Practice26 {

	static boolean isAdult(Person person)
	{
		if (person.getAge() < 20)
		{
			return false;
		}
		return true;
	}

	public static void main(String[] args)
	{
		Person person = new Person("endo", 29);

		System.out.println(isAdult(person));
	}
}
