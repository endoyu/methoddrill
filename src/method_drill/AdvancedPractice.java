package method_drill;

public class AdvancedPractice {

	static double getDistanceFromOrigin(Point p)
	{
		Point p0 = new Point(0.0, 0.0);

		return Practice23.getDistanceBetweenTwoPoints(p0, p);
	}

	public static void main(String[] args)
	{
		Point p = new Point(2.0, 5.0);

		System.out.println(getDistanceFromOrigin(p));
	}
}
