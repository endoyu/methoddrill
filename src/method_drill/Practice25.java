package method_drill;

public class Practice25 {

	static void printMessage(Person person)
	{
		System.out.printf("こんにちは%sさん", person.getName());
	}

	public static void main(String[] args)
	{
		Person person = new Person("endo", 29);

		printMessage(person);
	}
}
