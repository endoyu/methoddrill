package method_drill;

public class Practice22 {

	static double getDistanceFromOrigin(Point p)
	{
		return Math.sqrt(p.x * p.x + p.y * p.y);
	}

	public static void main(String[] args)
	{
		Point p = new Point(2.0, 4.0);

		System.out.println(getDistanceFromOrigin(p));
	}
}
