package method_drill;

public class Practice14 {

	static double getAbsoluteValue(double value) {

		if (value < 0) {
			return value * (-1);
		}
		return value;
	}

	public static void main(String[] args) {

		System.out.println(getAbsoluteValue(5.2));
		System.out.println(getAbsoluteValue(-3.6));

	}
}
